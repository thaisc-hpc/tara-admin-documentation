[![Gitlab](https://img.shields.io/badge/sources-gitlab-green.svg)](https://gitlab.com/thai-sc/tara-admin-documentation)[![Documentation Status](https://readthedocs.org/projects/tara-admin/badge/?version=latest)](https://tara-admin.readthedocs.io/en/latest/)

# ThaiSC Documentation

This repository contains documentation for ThaiSC

* [Online Reference Version](https://tara-admin.readthedocs.io/en/latest/) 

## Contributing

TBD

## Authors

* **Putt Sakdhnagool** - *Initial work* 

See also the list of [contributors](https://tara-admin.readthedocs.io/en/latest/contribute.html) who participated in this project.

## Issues / Feature request

You can submit bug / issues / feature request using [Tracker](https://gitlab.com/thai-sc/tara-admin-documentation/issues).

## License

TBD


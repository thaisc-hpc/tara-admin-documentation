=============
EasyBuild
=============

**Default robot path**

.. code::

    /utils/modules/software/EasyBuild/3.7.1/lib/python2.7/site-packages/easybuild_easyconfigs-3.7.1-py2.7.egg/easybuild/easyconfigs


OpenMPI
===========

The default OpenMPI easyconfig (``.eb`` files) does not enable Slurm and PMIx support by default. Following flags must be added to the easyconfig file.

.. code::

    configopts += '--with-slurm --with-pmi=/usr/include --with-pmi-libdir=/usr/lib64'

Note that different version of OpenMPI could have different ``configure`` script. For example, ``OpenMPI/2.1.2-GCC-6.4.0-2.28`` would require 

.. code:: 

    configopts += '--with-slurm --with-pmi=/usr --with-pmi-libdir=/usr'

OpenMPI installation can be verified by using ``ompi_info`` command. For example, to verify Slurm and PMIx installation use 

.. code::

    $ ompi_info | egrep -i 'slurm|pmi'

MPICH
=========

MPICH installation can be verified by using ``mpichversion`` or ``mpiexec --version``. 

Known Issues
==============

* OpenMPI 2.1.2 is known to have compatibility issue with PMI. OpenMPI 2.1.3 should be used instead.
    